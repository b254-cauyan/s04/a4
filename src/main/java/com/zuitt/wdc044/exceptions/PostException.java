package com.zuitt.wdc044.exceptions;


// This will be used to contain an exception (error) message during registration method of the UserController class.
public class PostException extends Exception{

    public PostException(String message) {
        super(message);
    }
    

}
